## Overview

Basic project to learn making a turn-based RPG.

Will slowly build it up. Not sure where it's going yet.

## Current Game Outline:

* turn-based RPG
* combat is always fighting single enemy
* dungeon-crawler? could be rogue-like
* move through rooms
* two-types damage: melee + magic; character can do either
* either can crit
* melee can bleed or stun
* magic can slow or daze
* simple inventory
* no economy - all loot (raw materials)
* use loot to improve weapon at blacksmiths
* spend XP to improve stats at shrines

## Copyright

Made with complete open license. Can be used in any capacity without attribution.

### Third-party Assets

Some sprites: Kenney Game Assets - https://kenney.itch.io/kenney-game-assets-1
Font: Original source unknown, taken from this tutorial by Brackeys - https://github.com/Brackeys/Dialogue-System
