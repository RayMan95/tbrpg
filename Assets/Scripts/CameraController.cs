using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject transformPlayerPosition;


    private readonly Vector3 cameraOffset = new Vector3(0, 0, -10);

    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector3(transformPlayerPosition.transform.position.x + cameraOffset.x, transformPlayerPosition.transform.position.y + cameraOffset.y, cameraOffset.z); // Camera follows the player with specified offset position
    }

    //void UpdateCamera()
    //{
    //    gameObject.transform.position = new Vector3(transformPlayerPosition.transform.position.x + cameraOffset.x, transformPlayerPosition.transform.position.y + cameraOffset.y, cameraOffset.z); // Camera follows the player with specified offset position
    //}
}
