using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public GameObject roomsParent; // holder gameobject for all rooms
    public GameObject closedRoomPrefab;
    public GameObject shrinePrefab;
    public GameObject blacksmithPrefab;
    public GameObject enemyPrefab;

    //[SerializeField] public LayerMask roomMask;
    int currentLevel = 1;
    private Dictionary<Vector3, GameObject> level = new();
    public Dictionary<Vector3, GameObject> Level 
    {
        get { return level; }
    }

    public Dictionary<Vector3, GameObject> enemies = new();
    public Dictionary<Vector3, GameObject> Enemies
    {
        get { return enemies; }
    }
    private int enemyId = 10; // Player always ID 0

    private LinkedList<GameObject> criticalPath = new();
    private LinkedList<GameObject> sidePath = new();

    private readonly float xMoveAmount = 13.5f;
    private readonly float yMoveAmount = 10;
    //private readonly int minXMoves = 2;
    private readonly int maxXMoves = 2;
    private int xMoves = 0;
    //private readonly int minYMoves = 3;
    private readonly int maxYMoves = 2;
    private int yMoves = 0;

    int direction;
    private float timeBtwnRoom;
    public float startTimeBtwnRoom = 0.75f;
    private bool makingCriticalPath = false;
    // private bool stopGeneration = false;
    //private int moveDirection;

    //bool hasShrine = false;
    //bool hasBlacksmith = false;

    public List<int[]> spawnPointReductions;

    public void Begin(int currLevel)
    {
        Debug.Log("Building level " + currentLevel);
        currentLevel = currLevel;

        if (currentLevel > 1)
        {
            ClearOldLevel();

        }

        makingCriticalPath = true;
        MakeFirstRoom();
        MakeCriticalPath();
        AddBlackSmith();
        //FillRooms();
        OpenRooms();
        //Destroy(gameObject)
    }

    private void ClearOldLevel()
    {
        // clear old level
        Debug.Log("Clearing old level");
        
        foreach (GameObject roomObject in level.Values)
        {
            Destroy(roomObject);
        }
        level.Clear();

        foreach (GameObject enemyObject in enemies.Values)
        {
            Destroy(enemyObject);
        }
        enemies.Clear();


        criticalPath.Clear();
        sidePath.Clear();

        xMoves = 0;
        yMoves = 0;
    }

    void MakeFirstRoom()
    {
        // Entrance room
        transform.position = new Vector3(0, 0, 0);
        AddRoom(transform.position, true);
        AddRoomType(transform.position, Room.RoomType.ENTRANCE);


        if (currentLevel > 1)
        {
            // entrance always shrine
            level.TryGetValue(transform.position, out GameObject roomGameObject);
            roomGameObject.GetComponent<Room>().roomTypes += (int)Room.RoomType.SHRINE;
            Instantiate(shrinePrefab, roomGameObject.transform);
        }
        
    }

    void MakeCriticalPath()
    {
        Debug.Log("Making critical path");

        timeBtwnRoom = startTimeBtwnRoom;
        direction = Random.Range(1, 4);

        while (makingCriticalPath)
        {
            if (timeBtwnRoom <= 0)
            {
                Move();
                timeBtwnRoom = startTimeBtwnRoom;
            }
            else
            {
                timeBtwnRoom -= Time.deltaTime;
            }
        }

        Debug.Log("CriticalPath count: " + criticalPath.Count);
    }

    private void AddRoom(Vector3 roomPosition, bool isCritical)
    {
        
        if (!RoomExists(roomPosition))
        {
            GameObject roomObject = Instantiate(closedRoomPrefab, roomPosition, Quaternion.identity);
            roomObject.transform.parent = roomsParent.transform;

            Room room = roomObject.GetComponent<Room>();
            room.onCriticalPath = isCritical;

            if (isCritical)
            {
                criticalPath.AddLast(roomObject);
            }
            else
            {
                sidePath.AddLast(roomObject);

            }

            level.Add(roomPosition, roomObject);
        }
    }

    // TODO: either make it so that it doesn't go down multiple levels or add flags to identify multiple downs 
    private void Move()
    {
        
        Vector3 oldPos = transform.position;

        if (direction == 1) // Move right
        {
            Vector3 newPos = new(transform.position.x + xMoveAmount, transform.position.y, transform.position.z);
            
            if (!RoomExists(newPos))
            {
                transform.position = newPos;

                AddRoom(newPos, true);

                // Add opening to current room before moving -- open right
                AddRoomOpening(oldPos, 2);

                // Add opening to new room after moving -- open left
                AddRoomOpening(newPos, 8);

                SetRoomActivity(true);

                xMoves++;
            }         
        }
        else if (direction == 2) // Move left
        {
            Vector3 newPos = new(transform.position.x - xMoveAmount, transform.position.y, transform.position.z);

            if (!RoomExists(newPos))
            { 
                transform.position = newPos;

                AddRoom(newPos, true);

                // Add opening to current room before moving -- open left
                AddRoomOpening(oldPos, 8);

                // Add opening to new room after moving -- open right
                AddRoomOpening(newPos, 2);

                SetRoomActivity(true);

                xMoves++;
            }
        }
        else // Move down (3 & 4) -- more likely to move down
        {
            Vector3 newPos = new(transform.position.x, transform.position.y - yMoveAmount, transform.position.z);

            if (!RoomExists(newPos))
            {
                if (xMoves == 0) // if no horizontal moves, fill out
                {
                    // first left
                    Vector3 sidePos = new(oldPos.x - xMoveAmount, transform.position.y, transform.position.z);

                    AddRoom(sidePos, false);

                    // Add opening to current room before moving -- open left
                    AddRoomOpening(oldPos, 8);

                    // Add opening to new room after moving -- open right
                    AddRoomOpening(sidePos, 2);

                    SetRoomActivity(false);

                    // then right
                    sidePos = new(oldPos.x + xMoveAmount, transform.position.y, transform.position.z);

                    AddRoom(sidePos, false);

                    // Add opening to current room before moving -- open right
                    AddRoomOpening(oldPos, 2);

                    // Add opening to new room after moving -- open left
                    AddRoomOpening(sidePos, 8);

                    SetRoomActivity(false);
                }
                else
                {
                    xMoves = 0;
                }


                transform.position = newPos;

                AddRoom(newPos, true);

                // Add opening to current room before moving -- open down
                AddRoomOpening(oldPos, 4);

                // Add opening to new room after moving -- open up
                AddRoomOpening(newPos, 1);

                SetRoomActivity(true);

                yMoves++;
                

                //direction = Random.Range(1, 5);
                if (yMoves == maxYMoves)
                {
                    // done
                    makingCriticalPath = false;

                    AddRoomType(newPos, Room.RoomType.EXIT);
                }
            }
        }

        if (xMoves == maxXMoves)
        {
            direction = 3;
        }
        else
        {
            direction = Random.Range(1, 4);
        }
        
    }

    private void SetRoomActivity(bool isCritical)
    {
        // TODO: potentially redesign/rebalance odds below

        level.TryGetValue(transform.position, out GameObject roomGameObject);
        Room room = roomGameObject.GetComponent<Room>();

        if (isCritical)
        {
            if (!room.roomTypes.HasFlag(Room.RoomType.ENTRANCE))
            {
                // skip entrance, all other critical path rooms are combat
                room.roomTypes += (int)Room.RoomType.COMBAT;
                SpawnEnemy(roomGameObject);
            }
        }
        else if (!room.roomTypes.HasFlag(Room.RoomType.ENTRANCE))
        {
            int type = Random.Range(1, 4);

            if (type >= 3)
            {
                // ~50% chance of additional combat -- should give better loot/xp
                room.roomTypes += (int)Room.RoomType.COMBAT;
                SpawnEnemy(roomGameObject);
            }
        }
        
    }

    private void SpawnEnemy(GameObject roomGameObject)
    {
        enemies.TryGetValue(transform.position, out GameObject enemyObject);

        if (enemyObject == null)
        {
            enemyObject = Instantiate(enemyPrefab, roomGameObject.transform);
            Enemy enemy = enemyObject.GetComponent<Enemy>();

            // TODO: AI generated
            enemy.Setup(enemyId, "Exit Enemy", currentLevel, 10, 1, 10, 1, 10, 1);
            enemyId++;

            enemies.Add(transform.position, enemyObject);
        }
    }

    private void AddRoomOpening(Vector3 position, int openingType)
    {
        level.TryGetValue(position, out GameObject roomGameObject);
        roomGameObject.GetComponent<Room>().roomOpenings += openingType;
    }

    private void AddRoomType(Vector3 position, Room.RoomType roomType)
    {
        level.TryGetValue(position, out GameObject roomGameObject);
        roomGameObject.GetComponent<Room>().roomTypes += (int)roomType;
    }

    public bool RoomExists(Vector3 roomPosition)
    {
        // Check if room at location exists
        level.TryGetValue(roomPosition, out GameObject roomObject);
        return roomObject != null;
    }

    private void OpenRooms()
    {
        Debug.Log("Opening rooms");

        foreach (GameObject roomObject in level.Values)
        {
            roomObject.GetComponent<Room>().MakeOpenings();
        }
    }

    private void AddBlackSmith()
    {
        GameObject exitRoomObject = criticalPath.Last.Value;
        Vector3 roomPosition = exitRoomObject.transform.position;

        GameObject nextRoomObject = null;

        while (nextRoomObject == null)
        {
            roomPosition.x += xMoveAmount;
            if (!RoomExists(roomPosition))
            {
                exitRoomObject.GetComponent<Room>().roomOpenings += (int)Room.RoomOpening.EAST;


                nextRoomObject = Instantiate(closedRoomPrefab, roomPosition, Quaternion.identity);
                GameObject blacksmithObject = Instantiate(blacksmithPrefab, roomPosition, Quaternion.identity);
                blacksmithObject.transform.parent = exitRoomObject.transform;
                
                Room nextRoom = nextRoomObject.GetComponent<Room>();
                nextRoom.roomTypes += (int)Room.RoomType.BLACKSMITH;
                nextRoom.roomOpenings += (int)Room.RoomOpening.WEST;
                
                level.Add(roomPosition, nextRoomObject);
                sidePath.AddLast(nextRoomObject);
            }
        }
    }

    // Returns bitmask of neighbours (will map to room type later)
    // returns 0 for no neighbours and null if invalid address 
    //private Room.Type FindCriticalNeighbours(Vector3 position, bool isCritical)
    //{
    //    if (!WithinBounds(position)) return Room.Type.fail;

    //    Room.Type result = 0;
    //    GameObject room;

    //    Vector3 modified = position;
    //    modified.x -= 10; // left

    //    if (level.TryGetValue(modified, out room) && room.GetComponent<Room>().onCriticalPath == isCritical) result += 1; // left

    //    modified = position;
    //    modified.x += 10; // right

    //    if (level.TryGetValue(modified, out room) && room.GetComponent<Room>().onCriticalPath == isCritical) result += 2; // right

    //    modified = position;
    //    modified.y += 10; // up

    //    if (level.TryGetValue(modified, out room) && room.GetComponent<Room>().onCriticalPath == isCritical) result += 4; // up

    //    modified = position;
    //    modified.y -= 10; // down

    //    if (level.TryGetValue(modified, out room) && room.GetComponent<Room>().onCriticalPath == isCritical)
    //    {
    //        // needs to have at most 1 flag set prior
    //        if (!(result.HasFlag(Room.Type.r)) && !(result.HasFlag(Room.Type.l)) || result == 0)
    //            result += 8; // down
    //    }
    //    return result;
    //}


}
