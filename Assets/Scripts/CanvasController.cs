using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{

    [HideInInspector]
    public static CanvasController instance;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);

        }
        else
        {
            instance = this;
        }

        DontDestroyOnLoad(gameObject);
    }

    // Dirty and probably bad
    //private void Update()
    //{
    //    transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
    //}
}
