using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class GameController : MonoBehaviour
{
    [HideInInspector]
    public static GameController instance;

    public GameObject mainCameraObject;
    public GameObject levelControllerObject;
    private LevelController levelController;
    public GameObject levelGameObject;
    public GameObject dialogueControllerObject;
    private DialogueController dialogueController;
    public GameObject battleControllerObject;
    private BattleController battleController;

    public GameObject playerPrefab;
    GameObject playerObject;
    Player player;
    public Transform transformPlayerPosition;

    private readonly float xMoveAmount = 13.5f;
    private readonly float yMoveAmount = 10;

    //[HideInInspector]
    //public static BattleController battleController;
    public GameObject enemyPrefab;

    //public GameObject helperObject;
    //HelperController helperController;

    public bool combatEnabled = true;

    [HideInInspector]
    public static GameState currentGameState = GameState.START;

    public enum GameState 
    { 
        START, ROAMING, ENTER_COMBAT, LEFT_COMBAT, MENU, NEW_LEVEL, DIALOGUE
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);

        }
        else
        {
            instance = this;
        }

        //helperController = helperObject.GetComponent<HelperController>();

        DontDestroyOnLoad(gameObject);
        currentGameState = GameState.START;
    }

    void Start()
    {

        levelController = LevelController.instance;
        dialogueController = DialogueController.instance;


        // initially loading in on start of level

        // TODO: player set up elsewhere
        playerObject = Instantiate(playerPrefab, transformPlayerPosition);
        player = playerObject.GetComponent<Player>();
        player.Setup(0, "Ray", 1, 12, 5, 5, 2, 4, 2, -1, 1);

        PlayerPrefsHelper playerPrefsHelper = new();
        playerPrefsHelper.WritePlayerData(player);
        


        //DelayFor(2.0f);
        //StartBattle(1);
    }

    private void Update()
    {
        if (currentGameState == GameState.DIALOGUE)
        {
             //Do nothing??
        }

        else if (currentGameState == GameState.NEW_LEVEL)
        {
            transformPlayerPosition.position = new Vector3(0.0f, 0.0f, 0.0f);

            // TODO: Implement logic for shrine first
            PlayerAtShrine();

        }

        else if (currentGameState == GameState.LEFT_COMBAT)
        {
            // loading in after combat
            currentGameState = GameState.ROAMING;

            levelController.ClearEnemyAt(transformPlayerPosition.position);
            levelGameObject.SetActive(true);
            playerObject.SetActive(true);
            mainCameraObject.SetActive(true);

            playerObject = Instantiate(playerPrefab, transformPlayerPosition);
            player = playerObject.GetComponent<Player>();

            PlayerPrefsHelper playerPrefsHelper = new();
            player = playerPrefsHelper.ReadPlayerData(player);
        }

        else if (currentGameState == GameState.START || currentGameState == GameState.ROAMING)
        {

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // pause
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
            {
                TryMovePlayer(new Vector3(-xMoveAmount, 0, 0), Room.RoomOpening.WEST);
                
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
            {
                TryMovePlayer(new Vector3(xMoveAmount, 0, 0), Room.RoomOpening.EAST);
                
            }
            else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            {
                TryMovePlayer(new Vector3(0, yMoveAmount, 0), Room.RoomOpening.NORTH);
                
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
            {
                TryMovePlayer(new Vector3(0, -yMoveAmount, 0), Room.RoomOpening.SOUTH);
                
            }
        }
        else
        {

        }


    }

    private void PlayerAtShrine()
    {
        dialogueController.PlayerHealed();
    }

    void TryMovePlayer(Vector3 displacement, Room.RoomOpening throughOpening)
    {
        if (levelController.PlayerCanMoveTo(transformPlayerPosition.transform.position, transformPlayerPosition.transform.position + displacement, throughOpening))
        {

            Room.RoomType currentRoomType = levelController.GetRoomTypes(transformPlayerPosition.transform.position);
            Room.RoomType nextRoomType = levelController.GetRoomTypes(transformPlayerPosition.transform.position + displacement);

            if (nextRoomType.HasFlag(Room.RoomType.COMBAT))
            {
                transformPlayerPosition.Translate(displacement); // first move player

                if (combatEnabled)
                {
                    playerObject.SetActive(false);
                    mainCameraObject.SetActive(false);
                    levelGameObject.SetActive(false);

                    StartBattle(levelController.GetEnemyIdAt(transformPlayerPosition.transform.position));
                }
            }
            else if (nextRoomType.HasFlag(Room.RoomType.BLACKSMITH))
            {
                transformPlayerPosition.Translate(displacement);
                // TODO: add logic for blacksmith menu
            }
            else
            {
                transformPlayerPosition.Translate(displacement);
                //UpdateCamera();
            }
        }
        else
        {
            Debug.Log("Player can't move there");
        }
    }

    // Starts battle, takes in enemy ID to set up passing params
    void StartBattle(int battleEnemyID)
    {
        ChangeGameState(GameState.ENTER_COMBAT);

        

        // TODO: enemy creation elsewhere
        CreateEnemy(battleEnemyID, "Ogre", 1, 10, 5, 6, 2, 5, 1, 10, new Vector3(1, 1, -20));
        //enemyObjects.TryGetValue(battleEnemyID, out GameObject enemyObject);

        //yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("SceneBattle", LoadSceneMode.Single);
    }

    // Create enemy and add to dictionary
    void CreateEnemy(int id, string charName, int lvl, int hp, float hpLvlMultiplier, int strength, float strengthLvlMultiplier, int intelligence, float intelligenceLvlMultiplier, int xp, Vector3 location)
    {
        GameObject enemyObject = Instantiate(enemyPrefab, location, Quaternion.identity);
        Enemy enemy = enemyObject.GetComponent<Enemy>();
        // TODO: pull enemy values from some kind of template/sheet
        enemy.Setup(id, charName, lvl, hp, hpLvlMultiplier, strength, strengthLvlMultiplier, intelligence, intelligenceLvlMultiplier, xp);
        
        PlayerPrefsHelper playerPrefsHelper = new();
        playerPrefsHelper.WriteEnemyData(enemy);
    }

    //IEnumerable DelayFor(float time)
    //{
    //    yield return new WaitForSeconds(time);
    //}

    private void ChangeGameState (GameState newState)
    {
        Debug.Log("Entering " + newState);
        currentGameState = newState;
    }

    

}
