using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public int ID;
    public string characterName;
    public int level;
    public float currentHp;

    private Dictionary<CharacterStat.CharacterStatName, CharacterStat> characterStats;


    public void Setup(int id, string charName, int lvl, float baseHp, float hpLvlMultiplier, float strength, float strengthLvlMultiplier, float intelligence, float intelligenceLvlMultiplier)
    {
        ID = id;
        characterName = charName;
        level = lvl;

        characterStats = new Dictionary<CharacterStat.CharacterStatName, CharacterStat>
        {
            { CharacterStat.CharacterStatName.HP, new CharacterStat(0, CharacterStat.CharacterStatName.HP, baseHp, hpLvlMultiplier) },
            { CharacterStat.CharacterStatName.STRENGTH, new CharacterStat(1, CharacterStat.CharacterStatName.STRENGTH, strength, strengthLvlMultiplier) },
            { CharacterStat.CharacterStatName.INTELLIGENCE, new CharacterStat(2, CharacterStat.CharacterStatName.INTELLIGENCE, intelligence, intelligenceLvlMultiplier) }
        };

        currentHp = GetStatCurrentValue(CharacterStat.CharacterStatName.HP);
    }

    internal bool GetHit(float amount)
    {
        currentHp -= amount;
        return currentHp <= 0;
    }

    public float GetStatBaseValue(CharacterStat.CharacterStatName statName)
    {
        characterStats.TryGetValue(statName, out CharacterStat characterStat);

        if (characterStat != null)
        {
            return characterStat.GetBaseValue(level);
        }
        else
        {
            return -1;
        }
    }

    public float GetStatCurrentValue(CharacterStat.CharacterStatName statName)
    {
        characterStats.TryGetValue(statName, out CharacterStat characterStat);
        
        if (characterStat != null)
        {
            return characterStat.GetValue(level);
        }
        else
        {
            return -1;
        }
    }

    public float GetStatModifier(CharacterStat.CharacterStatName statName)
    {
        characterStats.TryGetValue(statName, out CharacterStat characterStat);

        if (characterStat != null)
        {
            return characterStat.GetModifier();
        }
        else
        {
            return -1;
        }
    }



}
