using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BattleController : MonoBehaviour
{

    [HideInInspector]
    public static BattleController instance;

    private BattleState battleState;

    //[HideInInspector]
    public GameObject playerPrefab;
    public Transform playerBattlePosition;
    GameObject playerObject;
    Player player;
    public Text textPlayerName;
    public Text textPlayerLevel;
    public Text textPlayerMaxHp;
    public Text textPlayerCurrentHp;
    public Text textXPPercent;
    bool playerIsDead;

    public GameObject enemyPrefab;
    public Transform enemyBattlePosition;
    GameObject enemyObject;
    Enemy enemy;
    public Text textEnemyName;
    public Text textEnemyLevel;
    public Text textEnemyMaxHp;
    public Text textEnemyCurrentHp;
    bool enemyIsDead;

    public Button buttonPlayerStrengthAttack;
    public Button buttonPlayerIntelligenceAttack;
    public Button buttonPlayerFlee;
    public GameObject canvasLoseScreen;
    public GameObject canvasBattleScreen;


    //public GameObject helperObject;
    //HelperController helperController;

    enum BattleState { INACTIVE, START, PLAYER_TURN, ENEMY_TURN, FLEE, WON, LOST}
    enum Buttons { ATTACK, RUN };

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);

        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    { 
        battleState = BattleState.START;
        SetupBattle();
    }

    void Update()
    {
        if (GameController.currentGameState == GameController.GameState.DIALOGUE)
        {
            // Do nix
        }
    }

    public void SetupBattle()
    {
        //helperController.LogInformation("Setting up battle...");
        Debug.Log("Setting up battle...");

        SetupCharacters();
        
        //helperController.LogInformation("Done setting up battle");
        Debug.Log("Done setting up battle");
    }

    private void SetupCharacters()
    {
        PlayerPrefsHelper playerPrefsHelper = new();

        // Player

        playerObject = Instantiate(playerPrefab, playerBattlePosition);
        player = playerObject.GetComponent<Player>();
        player = playerPrefsHelper.ReadPlayerData(player);
        textPlayerName.text = player.characterName;
        textPlayerLevel.text = "" + player.level;
        textPlayerMaxHp.text = "/ " + player.GetStatCurrentValue(CharacterStat.CharacterStatName.HP) + "";
        textPlayerCurrentHp.text = "" + player.currentHp;
        textXPPercent.text = "" + Mathf.FloorToInt(player.CurrentXpToNextLevelPercent()) + "%";
        playerIsDead = false;

        // Enemy

        enemyObject = Instantiate(enemyPrefab, enemyBattlePosition);
        enemy = enemyObject.GetComponent<Enemy>();
        enemy = playerPrefsHelper.ReadEnemyData(enemy);
        textEnemyName.text = enemy.characterName;
        textEnemyLevel.text = "" + enemy.level;
        textEnemyMaxHp.text = "/ " + enemy.GetStatCurrentValue(CharacterStat.CharacterStatName.HP) + "";
        textEnemyCurrentHp.text = "" + enemy.currentHp;
        enemyIsDead = false;
    }

    public void ButtonStrengthAttackPressed()
    {
        CharacterAttack(true, CharacterStat.CharacterStatName.STRENGTH);
    }

    public void ButtonIntelligenceAttackPressed()
    {
        CharacterAttack(true, CharacterStat.CharacterStatName.INTELLIGENCE);
    }

    void CharacterAttack(bool isPlayer, CharacterStat.CharacterStatName stat)
    {
        

        if (isPlayer)
        {
            float dmg = player.GetStatCurrentValue(stat);

            enemyIsDead = enemy.GetHit(dmg);

            Debug.Log(player.characterName + " performed " + stat + " attack - hit " + enemy.characterName + " for " + dmg);

            UpdateHud(isPlayer);

            if (enemyIsDead)
            {
                battleState = BattleState.WON;
                EndBattle();
            }
            else
            {
                battleState = BattleState.ENEMY_TURN;
                EnemyTurn();
            }
        }
        else
        {
            float dmg = enemy.GetStatCurrentValue(stat);

            playerIsDead = player.GetHit(dmg);

            Debug.Log(enemy.characterName + " performed " + stat + " attack - hit " + player.characterName + " for " + dmg);

            UpdateHud(isPlayer);

            if (playerIsDead)
            {
                battleState = BattleState.LOST;
                EndBattle();
            }
            else
            {
                battleState = BattleState.PLAYER_TURN;
                //PlayerTurn();
            }
        }

        
    }

    public void ButtonFlee ()
    {
        Debug.Log("Player is attempting to flee...");
        // todo: dont disable whole canvas
        canvasBattleScreen.SetActive(false);
        DialogueController.instance.AttemptFlee();

        //battleState = BattleState.FLEE;
        //EndBattle();
    }

    void EnemyTurn()
    {
        // TODO AI
        
        int attackType = Random.Range(1, 3);
        switch (attackType)
        {
            case 1:
                CharacterAttack(false, CharacterStat.CharacterStatName.STRENGTH);
                break;
            case 2:
                CharacterAttack(false, CharacterStat.CharacterStatName.INTELLIGENCE);
                break;
            default:
                Debug.Log("False enemy attack");
                break;
        }
    }    

    void EndBattle()
    {
        // TODO all the GUI/animations/etc.

        buttonPlayerStrengthAttack.interactable = false;
        buttonPlayerIntelligenceAttack.interactable = false;
        buttonPlayerFlee.interactable = false;

        
        if (battleState == BattleState.WON)
        {
            // Win sequence
            int xp = enemy.GrantXp();
            player.GainXP(xp);
            textXPPercent.text = "" + player.CurrentXpToNextLevelPercent() + "%";

        }
        else if (battleState == BattleState.LOST)
        {
            // Lose sequence
            enemy.gameObject.SetActive(false);
            player.gameObject.SetActive(false);
            canvasBattleScreen.SetActive(false);
            canvasLoseScreen.SetActive(true);
        }
        else if (battleState == BattleState.FLEE)
        {
            //Debug.Log("Player int: " + player.GetStatCurrentValue(CharacterStat.CharacterStatName.INTELLIGENCE) + "; Enemy str: " + enemy.GetStatCurrentValue(CharacterStat.CharacterStatName.STRENGTH));

            float dmg = Mathf.Floor(enemy.GetStatCurrentValue(CharacterStat.CharacterStatName.STRENGTH) / 2);

            if (player.Flee(enemy.GetStatCurrentValue(CharacterStat.CharacterStatName.STRENGTH)))
            {
                // player flees successfully and takes half dmg
                player.GetHit(Mathf.Floor(dmg/2));
                Debug.Log("Player flees successfully");
                // TODO: post battle message
            }

            else
            {
                player.GetHit(dmg);
                Debug.Log("Player takes " + dmg + " damage");
                // TODO: post battle message

            }
        }


        // Write data to memory before ending battle
        PlayerPrefsHelper playerPrefsHelper = new();
        playerPrefsHelper.WritePlayerData(player);

        GameController.currentGameState = GameController.GameState.LEFT_COMBAT; 
        SceneManager.LoadScene(2);

    }

    public void EndDialogue()
    {
        battleState = BattleState.FLEE;
        EndBattle();
    }

    void UpdateHud(bool isPlayerTurn)
    {
        if (isPlayerTurn)
        {
            if (enemyIsDead)
            {
                textEnemyCurrentHp.text = "0";

            }
            else
            {
                textEnemyCurrentHp.text = "" + enemy.currentHp;
            }

        }
        else
        {
            if (playerIsDead)
            {
                textPlayerCurrentHp.text = "0";

            }
            else
            {
                textPlayerCurrentHp.text = "" + player.currentHp;
            }
        }
    }
}
