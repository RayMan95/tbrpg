using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    //[SerializeField]
    public RoomOpening roomOpenings;
    //[SerializeField]
    public RoomType roomTypes;

    // Length of sides of room
    //private readonly int height = 10;
    //private readonly int width = 13;
    
    public bool onCriticalPath = false;
    public Sprite[] doorSprites; // doorN, doorE, doorS, doorW, entrance, exit

    public GameObject[] removableWallObjects; // N, E, S, W
    //private Dictionary<Vector3, GameObject> borderSpawnPoints;

    public enum RoomOpening 
    {
        UNDEFINED = 0,
        NORTH = 1,
        EAST = 2,
        SOUTH = 4,
        WEST = 8
    }
    public enum RoomType 
    {
        UNDEFINED = 0,
        EMPTY = 1, 
        ENTRANCE = 2, 
        EXIT = 4, 
        COMBAT = 8, 
        BLACKSMITH = 16, 
        SHRINE = 32 
    }

    public void MakeOpenings()
    {
        // Add door sprites
        if (roomOpenings.HasFlag(RoomOpening.NORTH))
        {
            removableWallObjects[0].GetComponent<SpriteRenderer>().sprite = doorSprites[0];
        }
        if (roomOpenings.HasFlag(RoomOpening.EAST))
        {
            removableWallObjects[1].GetComponent<SpriteRenderer>().sprite = doorSprites[1];
        }
        if (roomOpenings.HasFlag(RoomOpening.SOUTH))
        {
            removableWallObjects[2].GetComponent<SpriteRenderer>().sprite = doorSprites[2];
        }
        if (roomOpenings.HasFlag(RoomOpening.WEST))
        {
            removableWallObjects[3].GetComponent<SpriteRenderer>().sprite = doorSprites[3];
        }

        // Add entrance/exit sprites
        if (roomTypes.HasFlag(RoomType.ENTRANCE))
        {
            removableWallObjects[0].GetComponent<SpriteRenderer>().sprite = doorSprites[4];
        }
        if (roomTypes.HasFlag(RoomType.EXIT))
        {
            removableWallObjects[2].GetComponent<SpriteRenderer>().sprite = doorSprites[5];
        }

    }
}
