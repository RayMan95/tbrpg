using UnityEngine;
using UnityEngine.UI;

public class DialogueController : MonoBehaviour
{
    public static DialogueController instance;
    public DialogueType currentDialogueType;

    public GameObject canvasObject;
    public Text textDialogue;

    private readonly string healed = "You are healed. I have healed you.";
    private readonly string attemptFlee = "Player attempted to flee... ";

    public enum DialogueType
    {
        COMBAT, ROAMING
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);

        }
        else
        {
            instance = this;
        }

        DontDestroyOnLoad(gameObject);
    }

    void StartDialogue(DialogueType dialogueType)
    {
        currentDialogueType = dialogueType;
        textDialogue.text = "";
        canvasObject.SetActive(true);
        canvasObject.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
        GameController.currentGameState = GameController.GameState.DIALOGUE;
    }

    public void EndDialogue()
    {
        textDialogue.text = "";
        canvasObject.SetActive(false);
        GameController.currentGameState = GameController.GameState.ROAMING;

        if (currentDialogueType == DialogueType.COMBAT)
        {
            // after flee
            BattleController.instance.EndDialogue();
        }
    }

    public void PlayerHealed()
    {
        StartDialogue(DialogueType.ROAMING);
        textDialogue.text = healed;
    }

    public void AttemptFlee()
    {
        StartDialogue(DialogueType.COMBAT);
        textDialogue.text = attemptFlee;
    }
}
