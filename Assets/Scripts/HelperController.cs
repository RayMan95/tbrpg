using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelperController : MonoBehaviour
{
    public GameObject debugUI;
    public Text textDebug;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F12))
        {
            if (debugUI.activeInHierarchy)
            {
                debugUI.SetActive(false);
            }
            else
            {
                debugUI.SetActive(true);
            }

        }
    }

    public void LogInformation(string log)
    {
        Debug.Log(log);
        string text = textDebug.text;
        text += "\n" + log;
        textDebug.text = text;
    }
}
