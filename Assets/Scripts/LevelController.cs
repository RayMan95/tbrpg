using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public static LevelController instance;

    public GameObject levelGeneratorObject;
    private Dictionary<Vector3, GameObject> level;
    private Dictionary<Vector3, GameObject> enemies;
    private int currentLevel;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);

        }
        else
        {
            instance = this;
        }

        currentLevel = 1;
        StartNewLevel();
    }

    void Start()
    {
        // store the pregenerated level locally
        level = levelGeneratorObject.GetComponent<LevelGenerator>().Level;
        enemies = levelGeneratorObject.GetComponent<LevelGenerator>().Enemies;

    }

    void StartNewLevel()
    {
        levelGeneratorObject.GetComponent<LevelGenerator>().Begin(currentLevel);
    }

    public bool PlayerCanMoveTo(Vector3 startPos, Vector3 nextPos, Room.RoomOpening throughOpening)
    {
        level.TryGetValue(startPos, out GameObject room);

        if (room != null)
        {
            level.TryGetValue(nextPos, out GameObject nextRoom);
            if (nextRoom != null && room.GetComponent<Room>().roomOpenings.HasFlag(throughOpening))
            {
                return true;
            }
            // TODO: not hardcode this vector comparison
            else if (room.GetComponent<Room>().roomTypes.HasFlag(Room.RoomType.EXIT) && (startPos.y - nextPos.y == 10f))
            {

                PlayerExited();
            }
        }

        return false;
    }

    public void ClearEnemyAt(Vector3 position)
    {
        enemies.TryGetValue(position, out GameObject enemyObject);

        if (enemyObject != null)
        {
            Destroy(enemyObject);

            level.TryGetValue(position, out GameObject roomObject);
            roomObject.GetComponent<Room>().roomTypes -= Room.RoomType.COMBAT;
        };

    }

    public int GetEnemyIdAt(Vector3 position)
    {
        enemies.TryGetValue(position, out GameObject enemyObject);

        if (enemyObject != null)
        {
            return enemyObject.GetComponent<Enemy>().ID;
        }

        return -1;
    }

    internal Room.RoomType GetRoomTypes(Vector3 position)
    {
        level.TryGetValue(position, out GameObject roomObject);

        if (roomObject != null)
        {
            return roomObject.GetComponent<Room>().roomTypes;
        }

        return Room.RoomType.UNDEFINED;
    }

    public void PlayerExited()
    {
        // TODO: animations/stats/etc. at end of level
        currentLevel++;
        GameController.currentGameState = GameController.GameState.NEW_LEVEL;
        StartNewLevel();

    }
}
