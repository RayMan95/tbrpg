using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsHelper
{
    #region strings
    // player
    readonly string _playerName = "PLAYERNAME";
    readonly string _playerLevel = "PLAYERLEVEL";
    readonly string _playerHp = "PLAYERMAXHP";
    readonly string _playerHpLevelMultiplier = "PLAYERHPMULTIPLIER";
    readonly string _playerCurrentHP = "PLAYERCURRENTHP";
    readonly string _playerStrength = "PLAYERSTRENGTH";
    readonly string _playerStrengthMultiplier = "PLAYERSTRENGTHMULTIPLIER";
    readonly string _playerIntelligence = "PLAYERINTELLIGENCE";
    readonly string _playerIntelligenceMultiplier = "PLAYERINTELLIGENCEMULTIPLIER";
    readonly string _playerCurrentXp = "PLAYERXP";

    // enemy
    readonly string _battleEnemyName = "BATTLEENEMYNAME";
    readonly string _battleEnemyLevel = "BATTLEENEMYLEVEL";
    readonly string _battleEnemyHp = "BATTLEENEMYMAXHP";
    readonly string _battleEnemyHpLevelMultiplier = "BATTLEENEMYHPMULTIPLIER";
    readonly string _battleEnemyStrength = "BATTLEENEMYSTRENGTH";
    readonly string _battleEnemyStrengthMultiplier = "BATTLEENEMYSTRENGTHMULTIPLIER";
    readonly string _battleEnemyIntelligence = "BATTLEENEMYINTELLIGENCE";
    readonly string _battleEnemyIntelligenceMultiplier = "BATTLEENEMYINTELLIGENCEMULTIPLIER";
    readonly string _battleEnemyXpGiven= "BATTLEENEMYXPGIVEN";
    #endregion

    public void SetName(bool isPlayer, string value)
    {
        if (isPlayer)
        {
            PlayerPrefs.SetString(_playerName, value);
        }

        else
        {
            PlayerPrefs.SetString(_battleEnemyName, value);
        }
    }
    public string GetName(bool isPlayer)
    {
        if (isPlayer)
        {
            return PlayerPrefs.GetString(_playerName);
        }

        else
        {
            return PlayerPrefs.GetString(_battleEnemyName);
        }
    }

    public void SetLevel(bool isPlayer, int value)
    {
        if (isPlayer)
        {
            PlayerPrefs.SetInt(_playerLevel, value);
        }

        else
        {
            PlayerPrefs.SetInt(_battleEnemyLevel, value);
        }
    }
    public int GetLevel(bool isPlayer)
    {
        if (isPlayer)
        {
            return PlayerPrefs.GetInt(_playerLevel);
        }

        else
        {
            return PlayerPrefs.GetInt(_battleEnemyLevel);
        }
    }

    public void SetBaseHp(bool isPlayer, float value)
    {
        if (isPlayer)
        {
            PlayerPrefs.SetFloat(_playerHp, value);
        }

        else
        {
            PlayerPrefs.SetFloat(_battleEnemyHp, value);
        }
    }
    public float GetBaseHp(bool isPlayer)
    {
        if (isPlayer)
        {
            return PlayerPrefs.GetFloat(_playerHp);
        }

        else
        {
            return PlayerPrefs.GetFloat(_battleEnemyHp);
        }
    }

    public void SetHpLevelMultiplier(bool isPlayer, float value)
    {
        if (isPlayer)
        {
            PlayerPrefs.SetFloat(_playerHpLevelMultiplier, value);
        }

        else
        {
            PlayerPrefs.SetFloat(_battleEnemyHpLevelMultiplier, value);
        }
    }
    public float GetHpLevelMultiplier(bool isPlayer)
    {
        if (isPlayer)
        {
            return PlayerPrefs.GetFloat(_playerHpLevelMultiplier);
        }

        else
        {
            return PlayerPrefs.GetFloat(_battleEnemyHpLevelMultiplier);
        }
    }

    public void SetStrength(bool isPlayer, float value)
    {
        if (isPlayer)
        {
            PlayerPrefs.SetFloat(_playerStrength, value);
        }

        else
        {
            PlayerPrefs.SetFloat(_battleEnemyStrength, value);
        }
    }
    public float GetStrength(bool isPlayer)
    {
        if (isPlayer)
        {
            return PlayerPrefs.GetFloat(_playerStrength); ;
        }

        else
        {
            return PlayerPrefs.GetFloat(_battleEnemyStrength);
        }
    }

    public void SetStrengthMultiplier(bool isPlayer, float value)
    {
        if (isPlayer)
        {
            PlayerPrefs.SetFloat(_playerStrengthMultiplier, value);
        }

        else
        {
            PlayerPrefs.SetFloat(_battleEnemyStrengthMultiplier, value);
        }
    }
    public float GetStrengthMultiplier(bool isPlayer)
    {
        if (isPlayer)
        {
            return PlayerPrefs.GetFloat(_playerStrengthMultiplier);
        }

        else
        {
            return PlayerPrefs.GetFloat(_battleEnemyStrengthMultiplier);
        }
    }

    public void SetIntelligence(bool isPlayer, float value)
    {
        if (isPlayer)
        {
            PlayerPrefs.SetFloat(_playerIntelligence, value);
        }

        else
        {
            PlayerPrefs.SetFloat(_battleEnemyIntelligence, value);
        }
    }
    public float GetIntelligence(bool isPlayer)
    {
        if (isPlayer)
        {
            return PlayerPrefs.GetFloat(_playerIntelligence);
        }

        else
        {
            return PlayerPrefs.GetFloat(_battleEnemyIntelligence);
        }
    }

    public void SetIntelligenceMultiplier(bool isPlayer, float value)
    {
        if (isPlayer)
        {
            PlayerPrefs.SetFloat(_playerIntelligenceMultiplier, value);
        }

        else
        {
            PlayerPrefs.SetFloat(_battleEnemyIntelligenceMultiplier, value);
        }
    }
    public float GetIntelligenceMultiplier(bool isPlayer)
    {
        if (isPlayer)
        {
            return PlayerPrefs.GetFloat(_playerIntelligenceMultiplier);
        }

        else
        {
            return PlayerPrefs.GetFloat(_battleEnemyIntelligenceMultiplier);
        }
    }

    #region Player Specific

    public void SetPlayerCurrentHp(float value)
    {
        PlayerPrefs.SetFloat(_playerCurrentHP, value);
    }

    public float GetPlayerCurrentHp()
    {
        return PlayerPrefs.GetFloat(_playerCurrentHP);
    }

    public void SetPlayerCurrentXp(int value)
    {
        PlayerPrefs.SetInt(_playerCurrentXp, value);
    }

    public int GetPlayerCurrentXp()
    {
        return PlayerPrefs.GetInt(_playerCurrentHP);
    }

    #endregion

    #region Enemy Specific

    private void SetEnemyXpGiven(int xp)
    {
        PlayerPrefs.SetInt(_battleEnemyXpGiven, xp);
    }

    private int GetEnemyXpGiven()
    {
        return PlayerPrefs.GetInt(_battleEnemyXpGiven);
    }

    #endregion

    #region ReadData

    // Reads Player data from memory
    public Player ReadPlayerData(Player player)
    {
        string name = GetName(true);
        int level = GetLevel(true);
        float hp = GetBaseHp(true);
        float hpLevelMultiplier = GetHpLevelMultiplier(true);
        float currentHP = GetPlayerCurrentHp();
        float strength = GetStrength(true);
        float strengthMultiplier = GetStrengthMultiplier(true);
        float intelligence = GetIntelligence(true);
        float intelligenceMultiplier = GetIntelligenceMultiplier(true);
        int currentXp = GetPlayerCurrentXp();

        player.Setup(0, name, level, hp, hpLevelMultiplier, strength, strengthMultiplier, intelligence, intelligenceMultiplier, currentHP, currentXp);

        return player;
    }

    // Reads Enemy data from memory
    public Enemy ReadEnemyData(Enemy enemy)
    {
        string name = GetName(false);
        int level = GetLevel(false);
        float hp = GetBaseHp(false);
        float hpLevelMultiplier = GetHpLevelMultiplier(false);
        float strength = GetStrength(false);
        float  strengthMultiplier = GetStrengthMultiplier(false);
        float intelligence = GetIntelligence(false);
        float intelligenceMultiplier = GetIntelligenceMultiplier(false);
        int xpGiven = GetEnemyXpGiven();

        enemy.Setup(0, name, level, hp, hpLevelMultiplier, strength, strengthMultiplier, intelligence, intelligenceMultiplier, xpGiven);

        return enemy;
    }

    #endregion

    #region WriteData

    // Writes Player data to memory
    public void WritePlayerData(Player player)
    {
        SetName(true, player.characterName);
        SetPlayerCurrentHp(player.currentHp);
        SetBaseHp(true, player.GetStatBaseValue(CharacterStat.CharacterStatName.HP));
        SetHpLevelMultiplier(true, player.GetStatModifier(CharacterStat.CharacterStatName.HP));
        SetLevel(true, player.level);
        SetStrength(true, player.GetStatBaseValue(CharacterStat.CharacterStatName.STRENGTH));
        SetStrengthMultiplier(true, player.GetStatModifier(CharacterStat.CharacterStatName.STRENGTH));
        SetIntelligence(true, player.GetStatBaseValue(CharacterStat.CharacterStatName.INTELLIGENCE));
        SetIntelligenceMultiplier(true, player.GetStatModifier(CharacterStat.CharacterStatName.INTELLIGENCE));
        SetPlayerCurrentXp(player.XP);
    }

    // Writes Enemy data to memory
    public void WriteEnemyData(Enemy enemy)
    {
        SetName(false, enemy.characterName);
        SetBaseHp(false, enemy.GetStatBaseValue(CharacterStat.CharacterStatName.HP));
        SetHpLevelMultiplier(false, enemy.GetStatModifier(CharacterStat.CharacterStatName.HP));
        SetLevel(false, enemy.level);
        SetStrength(false, enemy.GetStatBaseValue(CharacterStat.CharacterStatName.STRENGTH));
        SetStrengthMultiplier(false, enemy.GetStatModifier(CharacterStat.CharacterStatName.STRENGTH));
        SetIntelligence(false, enemy.GetStatBaseValue(CharacterStat.CharacterStatName.INTELLIGENCE));
        SetIntelligenceMultiplier(false, enemy.GetStatModifier(CharacterStat.CharacterStatName.INTELLIGENCE));
        SetEnemyXpGiven(enemy.xpGivenPerLevel);
    }
    #endregion
}
