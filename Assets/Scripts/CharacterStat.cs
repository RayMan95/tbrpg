using System;

public class CharacterStat
{
    public int ID;
    public CharacterStatName statName;
    public float baseValue;
    public float valueLvlModifier;

    public enum CharacterStatName { HP, STRENGTH, INTELLIGENCE, ARMOR, AURA }

    public CharacterStat(int id, CharacterStatName name, float baseVal, float lvlModifier)
    {
        ID = id;
        statName = name;
        baseValue = baseVal;
        valueLvlModifier = lvlModifier;
    }

    public float GetBaseValue(int level)
    {
        return baseValue;
    }

    public float GetValue (int level)
    {
        return baseValue + (valueLvlModifier * (level - 1));
    }

    public float GetModifier()
    {
        return valueLvlModifier;
    }
}