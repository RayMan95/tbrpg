using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    public int XP = 0;
    private int xpRequiredPerLevel = 100;
    public Vector2 roomPosition;

    public void Setup(int id, string charName, int lvl, float hp, float hpLvlMultiplier, float strength, float strengthLvlMultiplier, float intelligence, float intelligenceLvlMultiplier, float currHp, int currentXp)
    {
        base.Setup(id, charName, lvl, hp, hpLvlMultiplier, strength, strengthLvlMultiplier, intelligence, intelligenceLvlMultiplier);
        if (currHp == -1f)
        {
            // -1 is code for full HP
            currentHp = GetStatCurrentValue(CharacterStat.CharacterStatName.HP);
        }
        else
        {
            currentHp = currHp;
        }


        XP = currentXp;
        roomPosition = new Vector2(0, 0);
    }

    public int GainXP(int xpAmount)
    {
        XP += xpAmount;

        if (XP >= 100)
        {
            level++;
        }

        return XP;
    }

    public bool Flee(float enemyStr)
    {
        return GetStatCurrentValue(CharacterStat.CharacterStatName.INTELLIGENCE) > enemyStr;
    }

    public float CurrentXpToNextLevelPercent()
    {
        return ((XP * 100) / (level * xpRequiredPerLevel));
    }

    public void Heal()
    {
        // TODO: potentially rework
        currentHp = GetStatCurrentValue(CharacterStat.CharacterStatName.HP);
    }
}
