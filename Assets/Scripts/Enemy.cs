using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{

    public int xpGivenPerLevel;

    public void Setup(int id, string charName, int lvl, float hp, float hpLvlMultiplier, float strength, float strengthLvlMultiplier, float intelligence, float intelligenceLvlMultiplier, int xpGiven)
    {
        base.Setup(id, charName, lvl, hp, hpLvlMultiplier, strength, strengthLvlMultiplier, intelligence, intelligenceLvlMultiplier);
        xpGivenPerLevel = xpGiven;
    }

    public int GrantXp()
    {
        return xpGivenPerLevel * level;
    }
}
